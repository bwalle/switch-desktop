/* {{{
 * (c) 2014, Bernhard Walle <bernhard@bwalle.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. }}}
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdbool.h>
#include <getopt.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/input.h>
#include <assert.h>

#define ARRAY_SIZE(a) (sizeof(a)/sizeof((a)[0]))
#define max(a, b) (((a) < (b)) ? (b) : (a))

static char s_device[PATH_MAX];
static int s_desktops_per_row = -1;
static int s_desktops_per_column = -1;

static inline int mod(int a, int n)
{
	return (a % n + n) % n;
}

static void error(const char *msg, ...)
{
	va_list ap;

	va_start(ap, msg);
	vfprintf(stderr, msg, ap);
	va_end(ap);

	exit(EXIT_FAILURE);
}

static void usage(void)
{
	printf("Usage: switch-desktop -d <device>\n");
}

static void parse_commandline(int argc, char *argv[])
{
	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"help",             no_argument,         0,  'h' },
			{"device",           required_argument,   0,  'd' },
			{"desktops-per-row", required_argument,   0,  'r' },
			{0,                  0,                   0,   0  }
		};

		int c = getopt_long(argc, argv, "hr:d:", long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
		case 'h':
			usage();
			exit(EXIT_SUCCESS);

		case 'd':
			strncpy(s_device, optarg, PATH_MAX);
			s_device[PATH_MAX-1] = '\0';
			break;

		case 'r':
			s_desktops_per_row = atoi(optarg);
			break;

		default:
			printf("Invalid option specified (%c)", c);
			break;
		}
	}

	if (!*s_device)
		error("No device specified. The '-d <device>' option is mandatory!\n");
}

static int current_desktop(int *column, int *row)
{
	assert(column != NULL);
	assert(row != NULL);

	FILE *fp = popen("wmctrl -d", "re");
	if (!fp) {
		perror("Unable to start 'wmctrl -c'");
		return -1;
	}

	int max_desktop = -1;
	int desktop = -1;
	char *line = NULL;
	ssize_t read;
	size_t len = 0;
	while ((read = getline(&line, &len, fp)) != -1) {
		int number = -1;
		char star_or_not;
		if (sscanf(line, "%d  %c", &number, &star_or_not) != 2)
			continue;

		if (star_or_not == '*')
			desktop = number;

		max_desktop = max(max_desktop, number);
	}

	free(line);
	pclose(fp);

	if (s_desktops_per_row < 0)
		s_desktops_per_row = max_desktop + 1;
	s_desktops_per_column = (max_desktop + 1) / s_desktops_per_row;

	*column = mod(desktop, s_desktops_per_row);
	*row = desktop / s_desktops_per_row;

	return desktop;
}

static void switch_desktop(int desktop)
{
	printf("switch desktop %d\n", desktop);
	char cmd[1024];

	snprintf(cmd, sizeof(cmd), "wmctrl -s %d", desktop);
	int rc = system(cmd);
	if (rc != 0)
		fprintf(stderr, "Error: Unable to execute '%s': %d\n", cmd, WEXITSTATUS(rc));
}

static void switch_desktop_col_row(int column, int row)
{
	switch_desktop(row * s_desktops_per_row + column);
}

static void desktop_up(void)
{
	int col, row;
	int current = current_desktop(&col, &row);
	if (current == -1)
		return;

	switch_desktop_col_row(col, mod(row - 1, s_desktops_per_column));
}

static void desktop_down(void)
{
	int col, row;
	int current = current_desktop(&col, &row);
	if (current == -1)
		return;

	switch_desktop_col_row(col, mod(row + 1, s_desktops_per_column));
}

static void desktop_left(void)
{
	int col, row;
	int current = current_desktop(&col, &row);
	if (current == -1)
		return;

	switch_desktop_col_row(mod(col - 1, s_desktops_per_row), row);
}

static void desktop_right(void)
{
	int col, row;
	int current = current_desktop(&col, &row);
	if (current == -1)
		return;

	switch_desktop_col_row(mod(col + 1, s_desktops_per_row), row);
}

int main(int argc, char *argv[])
{
	parse_commandline(argc, argv);

	int fd = open(s_device, O_RDONLY|O_CLOEXEC);
	if (fd < 0)
		error("Unable to open '%s': %s\n", s_device, strerror(errno));

	bool win_pressed = false;
	while (true) {
		struct input_event ev;
		ssize_t ret = read(fd, &ev, sizeof(struct input_event));
		if (ret != sizeof(struct input_event))
			break;

		if (ev.type != EV_KEY)
			continue;

		switch (ev.code) {
		case KEY_LEFTMETA:
			win_pressed = ev.value;
			break;

		case KEY_1:
		case KEY_2:
		case KEY_3:
		case KEY_4:
		case KEY_5:
		case KEY_6:
		case KEY_7:
		case KEY_8:
		case KEY_9:
		case KEY_0:
			if (win_pressed && ev.value == 1)
				switch_desktop(ev.code - KEY_1);
			break;

		case KEY_UP:
			if (win_pressed && ev.value == 1)
				desktop_up();
			break;

		case KEY_DOWN:
			if (win_pressed && ev.value == 1)
				desktop_down();
			break;

		case KEY_LEFT:
			if (win_pressed && ev.value == 1)
				desktop_left();
			break;

		case KEY_RIGHT:
			if (win_pressed && ev.value == 1)
				desktop_right();
			break;

		default:
			break;
		}
	}
	
	close(fd);

	return EXIT_SUCCESS;
}
