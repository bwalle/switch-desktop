CFLAGS = -W -Wall -g

all: switch-desktop

switch-desktop: switch-desktop.c

.PHONY: install
install: switch-desktop
	install -m 0755 switch-desktop $(DESTDIR)/usr/bin

.PHONY: clean
clean:
	rm -f *.o switch-desktop
