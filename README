switch-desktop is a small C program to switch the virtual desktop.  It works
with the Linux event interface of the keyboard, which means that the program
works even when a virtual machine or rdesktop grabs all keys!

Installation
------------

  % make
  % sudo make install

This will install /usr/bin/switch-desktop. Use DESTDIR if you try to package
the program.


Usage
-----

 1. Make sure that /dev/input/* devices are accessible by your normal user.
    For example read [1].
 2. Find out which input device is your keyboard. Use 'evtest' for that.

    Example:

    % evtest
    No device specified, trying to scan all of /dev/input/event*
    Not running as root, no devices may be available.
    Available devices:
    /dev/input/event0:	Microsoft Microsoft® 2.4GHz Transceiver v7.0
    /dev/input/event1:	Microsoft Microsoft® 2.4GHz Transceiver v7.0
    /dev/input/event2:	Microsoft Microsoft® 2.4GHz Transceiver v7.0
    /dev/input/event3:	Logitech Unifying Device. Wireless PID:4002
    /dev/input/event4:	Power Button
    /dev/input/event5:	Power Button
    /dev/input/event6:	PC Speaker
    /dev/input/event7:	HDA Intel PCH HDMI/DP,pcm=3
    /dev/input/event8:	HDA Intel PCH Front Headphone
    /dev/input/event9:	HDA Intel PCH Line Out
    /dev/input/event10:	HDA Intel PCH Line
    /dev/input/event11:	HDA Intel PCH Rear Mic
    /dev/input/event12:	HDA Intel PCH Front Mic

    In that example, "Logitech Unifying Device. Wireless PID:4002" is the keyboard.
    But the number in /dev/input/event changes, so scan /dev/input/by-id for
    a symlink that points to devent3:

    % ls -l /dev/input/by-id
    lrwxrwxrwx 1 root root 9 25. Feb 10:11 usb-Logitech_USB_Receiver-if02-event-kbd -> ../event3
    lrwxrwxrwx 1 root root 9 25. Feb 10:11 usb-Microsoft_Microsoft®_2.4GHz_Transceiver_v7.0-event-kbd -> ../event0
    lrwxrwxrwx 1 root root 9 25. Feb 10:11 usb-Microsoft_Microsoft®_2.4GHz_Transceiver_v7.0-if01-event-mouse -> ../event1
    lrwxrwxrwx 1 root root 9 25. Feb 10:11 usb-Microsoft_Microsoft®_2.4GHz_Transceiver_v7.0-if01-mouse -> ../mouse0
    lrwxrwxrwx 1 root root 9 25. Feb 10:11 usb-Microsoft_Microsoft®_2.4GHz_Transceiver_v7.0-if02-event-kbd -> ../event2
    lrwxrwxrwx 1 root root 6 25. Feb 10:11 usb-Microsoft_Microsoft®_2.4GHz_Transceiver_v7.0-if02-kbd -> ../js0

    In that example, /dev/input/by-id/usb-Logitech_USB_Receiver-if02-event-kbd is the
    final device.

 3. Start "switch-desktop -d /dev/input/by-id/usb-Logitech_USB_Receiver-if02-event-kbd"
    automatically when your desktop starts up.

 4. If your desktop layout uses more lines and you want to use the UP and DOWN keys
    to work, set the -r <col_per_rows> parameter. So for example if you have following
    layout, you need '-r 3'.
        
          1 | 2 | 3
         ---+---+---
          4 | 5 | 6

 => Finished!


References
----------

[1] http://sourceforge.net/apps/mediawiki/gizmod/index.php?title=HOWTO_-_Setting_Input_Device_Permissions_-_Creating_a_udev_Rule
